###############################################################################
#   #     #   #   #     #   #    ##    #    ##    #   #     #   #   #     #   #
#    #   #    # #  #####  # #  ######  #   ####   #    #   #    # #  #####  # #
#   #######   # # ####### # # ######## #  ######  #   #######   # # ####### # #
#  ## ### ##  # ### ### ### # ## ## ## # ## ## ## #  ## ### ##  # ### ### ### #
# ########### # ########### # ######## # ######## # ########### # ########### #
# # ####### # #  #########  #   ####   #   #  #   # # ####### # #  #########  #
# # #     # # #   #     #   #  # ## #  #  # ## #  # # #     # # #   #     #   #
#    ## ##    #  #       #  # #      # # # #  # # #    ## ##    #  #       #  #
###############################################################################

ARG VERSION=stretch
FROM debian:$VERSION
MAINTAINER Andrew Dolbilin

###############################################################################
# Install necessary packages
###############################################################################
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    sudo \
    supervisor \
    nginx \
    apache2-utils \
    php-fpm \
    cron \
    syslog-ng \
    openssh-server \
    mysql-server \
    nano
RUN rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/*

###############################################################################
# Configure user
###############################################################################
ENV HOST_USERNAME "test"
ENV HOST_USERNAME_PASS 123
RUN useradd -m $HOST_USERNAME -s /bin/bash && echo "$HOST_USERNAME:$HOST_USERNAME_PASS" | chpasswd && adduser $HOST_USERNAME sudo

###############################################################################
# Configure sshd
###############################################################################
RUN mkdir /var/run/sshd && chmod 0755 /var/run/sshd && \
    sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd && \
    mkdir -p /home/$HOST_USERNAME/.ssh && chown -R $HOST_USERNAME:$HOST_USERNAME /home/$HOST_USERNAME/.ssh
COPY conf/ssh/authorized_keys /home/$HOST_USERNAME/.ssh/authorized_keys
COPY conf/ssh/sshd_config /etc/ssh/
VOLUME /home/$HOST_USERNAME/.ssh

###############################################################################
# Configure nginx
###############################################################################
RUN rm -rf /var/www/html/* && \
    mkdir -p /var/www/cert /var/www/log && \
    htpasswd -bc /var/www/html/.htpasswd admin admin
COPY conf/nginx/def.com /etc/nginx/sites-available/default
COPY conf/nginx/cert /var/www/cert

###############################################################################
# Configure php-fpm
###############################################################################
RUN mkdir -p /var/run/php
COPY conf/php-fpm/php.ini /etc/php/7.0/fpm/php.ini

###############################################################################
# Configure mysql
###############################################################################
RUN mkdir -p /var/run/mysqld && \
    mkdir -p /var/lib/mysql && \ 
    mysql_install_db && \
    chown -R mysql:root /var/lib/mysql && \
    chown -R mysql:root /var/run/mysqld
COPY conf/mysql/my.cnf /etc/mysql/mariadb.conf.d/50-server.cnf
COPY conf/mysql/manage.sh /tmp/
RUN chmod +x /tmp/manage.sh
RUN /tmp/manage.sh && \
    rm -f /tmp/manage.sh

###############################################################################
# Configure cron
###############################################################################
COPY conf/mysql/backup.sh /home/$HOST_USERNAME/
RUN chown $HOST_USERNAME:$HOST_USERNAME /home/$HOST_USERNAME/backup.sh && \
    chmod +x /home/$HOST_USERNAME/backup.sh
RUN echo "*/5 * 1 */6 *	$HOST_USERNAME	/home/$HOST_USERNAME/backup.sh" >> /etc/crontab

###############################################################################
# Configure supervisor
###############################################################################
COPY conf/supervisor/supervisord.conf /etc/supervisor/supervisord.conf
COPY conf/supervisor/sshd.conf /etc/supervisor/conf.d/sshd.conf
COPY conf/supervisor/cron.conf /etc/supervisor/conf.d/cron.conf
COPY conf/supervisor/nginx.conf /etc/supervisor/conf.d/nginx.conf
COPY conf/supervisor/mysql.conf /etc/supervisor/conf.d/mysql.conf
COPY conf/supervisor/php-fpm.conf /etc/supervisor/conf.d/php-fpm.conf
COPY conf/supervisor/syslog.conf /etc/supervisor/conf.d/syslog.conf
###############################################################################
# Configure content
###############################################################################
#COPY content/www/index.php /var/www/html/index.php
COPY content/www /var/www/html
VOLUME /var/www/html

###############################################################################
# Expose ports:
# 10022 - ssh
#    80 - http
#   443 - https
#  3306 - mysql
###############################################################################
EXPOSE 10022 80 443 3306

#CMD ["/usr/sbin/sshd", "-D", "-e", "-f", "/etc/ssh/sshd_config"]
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
