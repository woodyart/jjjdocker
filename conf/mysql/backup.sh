#!/bin/bash
USER="test"
PASS="123"

NOW="$(date +'%Y_%m_%d')"

BACKUPFOLDER="/tmp/"
LOGFILE="/tmp/backup_log_$NOW.log"

DATABASES=$(/usr/bin/mysql -u$USER -p$PASS -e"show schemas;" | awk 'NR>1 && ! /information_schema/ && ! /perfomance_schema/ {print $1}')

for DATABASE in $DATABASES
do
    echo "mysqldump started at $(date +'%d-%m-%Y %H:%M:%S') for $DATABASE" >> "$LOGFILE"
    /usr/bin/mysqldump -u$USER -p$PASS --default-character-set=utf8mb4 $DATABASE | gzip > $BACKUPFOLDER$DATABASE"_$NOW.gz"
    echo "mysqldump finished at $(date +'%d-%m-%Y %H:%M:%S')" >> "$LOGFILE"
done

exit 0 
