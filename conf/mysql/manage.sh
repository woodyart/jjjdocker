#!/bin/bash
# Starting mysql
echo "Starting mysql_safe... wait 10 sec"
nohup /usr/bin/mysqld_safe > /dev/null 2>&1 &
sleep 10s

echo "Preparing mysql... wait few sec"
mysqladmin -uroot password root
# Work with database
mysql -uroot -proot -e \
"CREATE DATABASE test1 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
 CREATE DATABASE test2 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
 CREATE DATABASE test3 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
 CREATE USER test@'%' IDENTIFIED BY '123';
 GRANT SELECT ON test1.* TO test@'%';
 GRANT SELECT ON test2.* TO test@'%';
 GRANT SELECT ON test3.* TO test@'%';
 FLUSH PRIVILEGES;
"
sleep 3s

# Stopping mysql
echo "Shutdown mysql... wait 10 sec"
killall mysqld
sleep 10s
