ssl_certificate /var/www/cert/def.com.cert;
ssl_certificate_key /var/www/cert/def.com.key;

server {
       listen         80 default_server;
       #server_name   def.com www.def.com;
       return         301 $scheme://www.$host$request_uri;
}

server {
       listen         443 ssl default_server;
       return         301 $scheme://www.$host$request_uri;
}


server {
        listen        443 ssl;
        server_name   www.def.com;
        ssl on;
        access_log    /var/www/log/access.log;
        error_log     /var/www/log/error.log;
        root          /var/www/html;
        index         index.php index.htm index.html;

        location /private {
            auth_basic  "Only registered users can see my nude photos. Use admin/admin to see ;)";
            auth_basic_user_file /var/www/html/.htpasswd;
        }

        location / {
		    try_files $uri $uri/ =404;
	    }

    	location ~ \.php$ {
	    	include snippets/fastcgi-php.conf;
		    fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
	    }
}
